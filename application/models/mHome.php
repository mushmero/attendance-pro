<?php defined('BASEPATH') OR exit('No direct script access allowed');

class mHome extends CI_Model {
	var $staff = 'staff';
	var $attendance = 'attendance';

	function __construct() {
		parent::__construct();
	}
	function checkStaff($staffno) {
		$query = $this->db->where('staffno', $staffno)->get($this->staff);
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	function insTimeIn($data) {
		$query = $this->db->insert($this->attendance, $data);
		return $this->db->insert_id();
	}
	function insTimeOut($data, $staffno) {
		$query = $this->db->set($data)->where('staffno', $staffno)->update($this->attendance);
		return $this->db->affected_rows();
	}
	function checkStaffExist($staffno, $date) {
		$query = $this->db->from($this->attendance)->where(array('staffno' => $staffno, 'datein' => $date))->get();
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}
	function updTimeIn($data, $staffno, $date) {
		$query = $this->db->set($data)->where(array('staffno' => $staffno, 'datein' => $date))->update($this->attendance);
		return $this->db->affected_rows();
	}
	function updTimeOut($data, $staffno, $date) {
		$query = $this->db->set($data)->where(array('staffno' => $staffno, 'datein' => $date))->update($this->attendance);
		return $this->db->affected_rows();
	}
}