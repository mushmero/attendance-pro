<?php defined('BASEPATH') OR exit('No direct script access allowed');

class mDashboard extends CI_Model {
	var $states = 'states';
	var $status = 'status';
	var $dept = 'department';
	var $staff = 'staff';
	var $level = 'levels';
	var $attendance = 'attendance';

	function __construct() {
		parent::__construct();
	}

	function stateData(){
		$states = [
				'JHR' => 'Johor',
			    'KDH' => 'Kedah',
			    'KTN' => 'Kelantan',
			    'MLK' => 'Melaka',
			    'NSN' => 'Negeri Sembilan',
			    'PHG' => 'Pahang',
			    'PRK' => 'Perak',
			    'PLS' => 'Perlis',
			    'PNG' => 'Pulau Pinang',
			    'SBH' => 'Sabah',
			    'SWK' => 'Sarawak',
			    'SGR' => 'Selangor',
			    'TRG' => 'Terengganu',
			    'KUL' => 'W.P. Kuala Lumpur',
			    'LBN' => 'W.P. Labuan',
			    'PJY' => 'W.P. Putrajaya',
				];
		return $states;
	}
	public function getStates() {
		$states = $this->stateData();
		return $states;
	}
	function getDepartment() {
		$query = $this->db->from($this->dept)->order_by('deptname', 'ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function getTagByDept($dept) {
		$query = $this->db->get_where($this->dept, array('id' => $dept), 1);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	function checkDepartment($name) {
		$query = $this->db->get_where($this->dept, array('deptname' => $name), 1);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	function insertDepartment($data) {
		$query = $this->db->insert($this->dept, $data);
		return $this->db->insert_id();
	}
	function removeDepartment($id) {
		$this->db->delete($this->dept, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	function getStaff() {
		$raw = "select a.id, a.staffno, a.name, a.gender, a.address, a.city, a.postcode, a.states, a.level, a.department, a.status, c.deptname, c.tag, d.level as levelName from $this->staff a  left join $this->dept c on a.department = c.id left join $this->level d on a.level = d.id order by name ASC";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function countAllStaff() {
		$raw = "select count(*) as count from staff where status = 'Active'";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
	function insertStaff($data) {
		$query = $this->db->insert($this->staff, $data);
		return $this->db->insert_id();
	}
	function getResultByID($id) {
		$raw = "select a.staffno, a.timein, a.timeout, a.datein, a.dateout, b.name, b.status from $this->attendance a left join $this->staff b on a.staffno = b.staffno where a.staffno = '$id' order by a.datein ASC";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function removeStaff($id) {
		$this->db->set('status','Inactive')->where('id', $id)->update($this->staff);
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	function getAllToday($date) {
		$raw = "select a.staffno, a.timein, a.timeout, a.datein, a.dateout, b.name, b.status from $this->attendance a left join $this->staff b on a.staffno = b.staffno where a.datein = '$date'";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function getReportData($filter){
		$query = "select b.name, b.status, a.staffno, a.timein, a.timeout, a.datein, a.dateout, c.deptname, d.level from $this->attendance a inner join $this->staff b on a.staffno = b.staffno inner join $this->dept c on b.department = c.id inner join $this->level d on b.level = d.id $filter";
		$rslt = $this->db->query($query);
		if ($rslt->num_rows() > 0) {
			return $rslt->result();
		} else {
			return false;
		}
	}
}