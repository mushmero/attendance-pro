<?php defined('BASEPATH') OR exit('No direct script access allowed');

class mUsers extends CI_Model {
	var $users = 'users';
	var $levels = 'levels';

	function __construct() {
		parent::__construct();
	}
	function checkUser($username) {
		$query = $this->db->get_where($this->users, array('username' => $username), 1);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}
	function insertUser($data) {
		$query = $this->db->insert($this->users, $data);
		return $this->db->insert_id();
	}
	function getUsers() {
		$raw = "select a.id, a.username, b.level from $this->users a left join $this->levels b on a.level = b.id order by a.id ASC";
		$query = $this->db->query($raw);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function removeUser($id) {
		$this->db->delete($this->users, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	function insertLevels($data) {
		$query = $this->db->insert($this->levels, $data);
		return $this->db->insert_id();
	}
	function checkLevels($levelname) {
		$query = $this->db->get_where($this->levels, array('level' => $levelname), 1);
		if ($query->num_rows() > 0) {
			return $query->row();
		} else {
			return false;
		}
	}
	function getLevels() {
		$query = $this->db->from($this->levels)->order_by('id', 'ASC')->get();
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}
	function removeLevel($id) {
		$this->db->delete($this->levels, array('id' => $id));
		if ($this->db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
}