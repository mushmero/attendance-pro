<?php defined('BASEPATH') OR exit('No direct script access allowed');

class passwordLib {
	var $max = 3600;
	private function generateSalt($length = 10) {
		$list = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$salt = "";
		while ($i < $length) {
			$salt .= $list[mt_rand(0, strlen($list) - 1)];
			$i++;
		}
		return $salt;
	}
	function hashPassword($password) {
		$salt = $this->generateSalt();
		return $salt . '.' . md5($salt . $password);
	}
	function verifyPassword($password, $hashedPassword) {
		list($salt, $hash) = explode('.', $hashedPassword);
		$true = $salt . '.' . md5($salt . $password);
		if($hashedPassword == $true){
			return true;
		}else{
			return false;
		}
	}
	function setLogin($data) {
		$CI = &get_instance();
		$CI->session->set_userdata(array('last_activity' => time(), 'logged_in' => 'yes', 'user' => $data));
	}
	function removeLogin() {
		$CI = &get_instance();
		$CI->session->unset_userdata('last_activity');
		$CI->session->unset_userdata('logged_in');
		$CI->session->unset_userdata('user');
	}
	function checkLogin() {
		$CI = &get_instance();
		$last_activity = $CI->session->userdata('last_activity');
		$logged_in = $CI->session->userdata('logged_in');
		$user = $CI->session->userdata('user');
		if ($logged_in == 'yes' && (time() - $last_activity) < $this->max) {
			$this->setLogin($user);
			return true;
		} else {
			$this->removeLogin();
			return false;
		}
	}
	function base64Encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}
	function base64Decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}
}