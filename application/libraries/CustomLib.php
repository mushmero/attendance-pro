<?php defined('BASEPATH') OR exit('No direct script access allowed');

class customLib {
	function moveElement(&$array, $a, $b) {
	    $out = array_splice($array, $a, 1);
	    array_splice($array, $b, 0, $out);
	}
}