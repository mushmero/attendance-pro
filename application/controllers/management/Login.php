<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
		$this->load->model('musers');
		$this->load->library('passwordlib');
	}
	public function index() {
		if ($this->passwordlib->checkLogin()) {
			redirect('dashboard/main');
		}
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			//if ($username == 'superadmin' && $password == 'superadmin') {
				//redirect('management/register');
			//} else {
				if ($user = $this->musers->checkUser($username)) {
					if ($this->passwordlib->verifyPassword($password, $user['password'])) {
						$this->passwordlib->setLogin($user);
						redirect('dashboard/main');
					} else {
						$this->data['error'] = 'Invalid Username or Password';
					}
				} else {
					$this->data['error'] = 'Username not found!';
				}
			//}
		}
		$this->load->view('management/login', $this->data);
	}
	public function logout() {
		$this->passwordlib->removeLogin();
		$this->data['success'] = 'You have successfully logout from system';
		$this->load->view('management/login', $this->data);
	}
	public function noaccess() {
		$this->data['error'] = 'You have no access or your login have expired';
		$this->load->view('management/login', $this->data);
	}
}