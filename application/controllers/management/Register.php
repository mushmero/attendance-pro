<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
		$this->load->model('musers');
		$this->load->library('passwordlib');
	}
	public function index() {
		$this->data['levels'] = $this->musers->getLevels();
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Password', 'required|matches[password]');
		$this->form_validation->set_rules('level', 'Level', 'required');

		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->passwordlib->hashPassword($this->input->post('password'));
			$level = $this->input->post('level');
			$data = array(
				'username' => $username,
				'password' => $password,
				'level' => $level,
				'logdate' => date('Y-m-d'),
				'logtime' => time(),
			);
			if (!$this->musers->checkUser($username)) {
				if ($this->musers->insertUser($data)) {
					$this->data['success'] = 'Successfully add new user';
				} else {
					$this->data['error'] = 'Unable to add new user';
				}
			} else {
				$this->data['error'] = 'User already exists';
			}
		}
		$this->data['users'] = $this->musers->getUsers();
		$this->load->view('management/register', $this->data);
	}
	public function levels() {
		$this->form_validation->set_rules('levelname', 'Level Name', 'required');
		if ($this->form_validation->run()) {
			$levelname = strtoupper($this->input->post('levelname'));
			if (!$this->musers->checkLevels($levelname)) {
				$data = array(
					'level' => $levelname,
				);
				$this->musers->insertLevels($data);
				$this->data['success'] = 'Successfully add new level (' . $levelname . ')';
			} else {
				$this->data['error'] = 'Level (' . $levelname . ') already exists';
			}
		}
		$this->data['level'] = $this->musers->getLevels();
		$this->load->view('management/level', $this->data);
	}
	public function removeuser($i) {
		if ($this->musers->removeUser($i)) {
			$this->data['success'] = 'Successfully removed user';
		} else {
			$this->data['error'] = 'Failed to remove user';
		}
		$this->load->view('/management/register', $this->data);
		header('Refresh:1, url=../../../management/register');
	}
	public function removelevel($i) {
		if ($this->musers->removeLevel($i)) {
			$this->data['success'] = 'Successfully removed level ';
		} else {
			$this->data['error'] = 'Failed to remove level';
		}
		$this->load->view('/management/level', $this->data);
		header('Refresh:1, url=../../../management/level');
	}
}