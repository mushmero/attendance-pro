<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	var $data = array();

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Kuala_Lumpur");
		$this->load->model('mhome');
	}

	public function index() {
		$this->form_validation->set_rules('staffid', 'Staff No', 'required');
		if ($this->form_validation->run()) {
			$staffno = strtoupper($this->input->post('staffid'));
			$in = $this->input->post('in');
			$out = $this->input->post('out');
			if ($this->mhome->checkStaff($staffno)) {
				if ($in != '') {
					if ($this->mhome->checkStaffExist($staffno, date('Y-m-d'))) {
						$data = array(
							'timein' => time(),
						);
						$this->mhome->updTimeIn($data, $staffno, date('Y-m-d'));
						$this->data['success'] = 'Successfully clock in.';
					} else {
						$data = array(
							'staffno' => $staffno,
							'timein' => time(),
							'datein' => date('Y-m-d'),
						);
						$this->mhome->insTimeIn($data);
						$this->data['success'] = 'Successfully clock in.';
					}
				} else if ($out != '') {
					if ($this->mhome->checkStaffExist($staffno, date('Y-m-d'))) {
						$data = array(
							'timeout' => time(),
							'dateout' => date('Y-m-d'),
						);
						$this->mhome->updTimeOut($data, $staffno, date('Y-m-d'));
						$this->data['success'] = 'Successfully clock out.';
					} else {
						$data = array(
							'staffno' => $staffno,
							'timeout' => time(),
							'dateout' => date('Y-m-d'),
						);
						$this->mhome->insTimeOut($data);
						$this->data['success'] = 'Successfully clock out.';
					}
				}
			} else {
				$this->data['error'] = 'Staff No not found! Please contact management immediately.';
			}
		}
		$this->load->view('home', $this->data);
	}
}