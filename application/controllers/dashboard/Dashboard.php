<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends CI_Controller {
	var $data = array();
	function __construct() {
		parent::__construct();
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$this->load->library('passwordlib');
		$this->load->library('customlib');
		$this->load->model('mdashboard');
		$this->load->model('musers');

		if ($this->passwordlib->checkLogin() === FALSE) {
			$this->passwordlib->removeLogin();
			redirect('management/login/noaccess');
		} else {
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index() {
		$this->data['countAllStaff'] = $this->mdashboard->countAllStaff();
		$this->load->view('dashboard/dashboard', $this->data);
	}
	private function generateRandomNo($length = 4) {
		$i = 0;
		$no = '';
		while ($i < $length) {
			$no .= mt_rand(0, 10);
			$i++;
		}
		return $no;
	}
	public function department() {
		$this->form_validation->set_rules('deptname', 'Department Name', 'required');
		$this->form_validation->set_rules('tag', 'Department Tag', 'required|alpha');
		if ($this->form_validation->run()) {
			$deptname = strtoupper($this->input->post('deptname'));
			$tag = strtoupper($this->input->post('tag'));
			if (!$this->mdashboard->checkDepartment($deptname)) {
				$data = array(
					'deptname' => $deptname,
					'tag' => $tag,
				);
				if ($this->mdashboard->insertDepartment($data)) {
					$this->data['success'] = 'New department have been added';
				} else {
					$this->data['error'] = 'Unable to add new department';
				}
			} else {
				$this->data['error'] = 'Department ' . $deptname . ' already exist.';
			}
		}
		$this->data['department'] = $this->mdashboard->getDepartment();
		$this->load->view('dashboard/department', $this->data);
	}
	public function removedepartment($i) {
		if ($this->mdashboard->removedepartment($i)) {
			$this->data['success'] = 'Successfully removed department';
		} else {
			$this->data['error'] = 'Successfully removed department';
		}
		$this->load->view('/dashboard/department', $this->data);
		header('Refresh:1, url=../../../dashboard/department');
	}
	public function staff() {
		$user = $this->session->userdata('user');
		$this->data['states'] = $this->mdashboard->getStates();
		$this->data['department'] = $this->mdashboard->getDepartment();
		$this->data['levels'] = $this->musers->getLevels();

		$this->form_validation->set_rules('name', 'Full Name', 'required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('postcode', 'Postcode', 'required|numeric');
		$this->form_validation->set_rules('states', 'State', 'required');
		$this->form_validation->set_rules('department', 'Department', 'required');
		$this->form_validation->set_rules('level', 'Level', 'required');

		if ($this->form_validation->run()) {
			$name = strtoupper($this->input->post('name'));
			$gender = strtoupper($this->input->post('gender'));
			$address = strtoupper($this->input->post('address'));
			$city = strtoupper($this->input->post('city'));
			$postcode = $this->input->post('postcode');
			$state = $this->input->post('states');
			$department = $this->input->post('department');
			$level = $this->input->post('level');
			$tag = $this->mdashboard->getTagByDept($department);
			$staffno = $tag['tag'] . date('y') . $this->generateRandomNo();

			$data = array(
				'staffno' => $staffno,
				'name' => $name,
				'gender' => $gender,
				'address' => $address,
				'city' => $city,
				'postcode' => $postcode,
				'states' => $state,
				'department' => $department,
				'level' => $level,
				'regdate' => date('Y-m-d'),
				'regtime' => time(),
				'reguser' => $user['id'],
			);
			if ($this->mdashboard->insertStaff($data)) {
				$this->data['success'] = 'Successfully add new staff';
			} else {
				$this->data['error'] = 'Unable to add new staff';
			}
		}
		$this->data['staff'] = $this->mdashboard->getStaff();
		$this->load->view('dashboard/staffs', $this->data);
	}
	public function removestaff($i) {
		if ($this->mdashboard->removeStaff($i)) {
			$this->data['success'] = 'Successfully removed staff';
		} else {
			$this->data['error'] = 'Successfully removed staff';
		}
		$this->load->view('/dashboard/staff', $this->data);
		header('Refresh:1, url=../../../dashboard/staff');
	}
	public function attendance() {
		$this->form_validation->set_rules('staffid', 'Staff ID', 'required');
		if ($this->form_validation->run()) {
			$staffid = strtoupper($this->input->post('staffid'));
			if ($result = $this->mdashboard->getResultByID($staffid)) {
				$this->data['result'] = $result;
			} else {
				$this->data['error'] = 'Sorry, Staff not found!';
			}
			$this->data['result'] = $this->mdashboard->getResultByID($staffid);
		}
		$today = $this->mdashboard->getAllToday(date('Y-m-d'));

		$this->data['all'] = $today;
		$this->load->view('dashboard/attendance', $this->data);
	}
	public function report() {
		$this->load->view('dashboard/report', $this->data);
	}

	public function reportData(){
		$select = $this->input->post('reportSelect');
		$date = $this->input->post('reportdate');
		$filter = '';
		if($select == 'all'){
			$filter = '';
		}elseif($select == 'other'){
			$filter = "where a.datein = '".date('Y-m-d', strtotime(str_replace('/','-',$date)))."'";
		}
		$data = $this->mdashboard->getReportData($filter);
		if(!empty($data)){
			$i = 1;
			foreach($data as $d){
				$d->id = $i;
				$d->datein = date('d/m/Y', strtotime($d->datein));
				$d->timein = date('h:i:s A', $d->timein);
				$d->timeout = date('h:i:s A', $d->timeout);
				$d->status = (date('H:i:s', strtotime($d->timein)) <= date('H:i:s', strtotime('09:00:00'))) ? 'Normal' : 'Late';
				$i++;
			}	
		}else{
			$data = array();
		}
		if($data){
			foreach($data as $d){
				// $keys = array_keys((array)$v);
				// $values[] = array_values((array)$v);
				$newdata[] = (array)$d;
			}
			for($i = 0; $i < count($newdata); $i++){			
				$arr[$i] = array($newdata[$i]['id'], $newdata[$i]['name'].' ('.$newdata[$i]['staffno'].')', $newdata[$i]['deptname'], $newdata[$i]['level'], $newdata[$i]['datein'], $newdata[$i]['timein'], $newdata[$i]['timeout'], $newdata[$i]['status'] );
			}
			echo json_encode($arr);
		}else{
			echo json_encode(array(1=> ''));
		}
	}
}