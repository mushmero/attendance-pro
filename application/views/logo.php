<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri2 = $this->uri->segment(1);?>
<?php $user = $this->session->userdata('user');?>

<!-- Sidebar toggle button-->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
</nav>