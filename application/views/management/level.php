<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>

<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><i class="fas fa-sticky-note"></i> Management</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-danger card-outline">
          <div class="card-header text-center">
            <h3 class="card-title">Level Name</h3>
          </div>
          <div class="card-body">
            <?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
            <?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
            <form action="<?php echo site_url('management/level'); ?>" method="POST">
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="levelname" placeholder="Level Name">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                <?php echo form_error('levelname', '<p class="help-inline">', '</p>'); ?>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Add</button>
                </div>
              </div>
            </form>            
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-danger card-outline">
          <div class="card-header text-center">
            <h3 class="card-title">List of Levels Available</h3>
          </div>
          <div class="card-body">
            <table class="table table-condensed">
              <tr>
                <th>#</th>
                <th>Level Name</th>
                <th>Action</th>
              </tr>
              <?php if (!empty($level)) {?>
                <?php $i = 1;foreach ($level as $l) {?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $l->level; ?></td>
                    <td><a href="<?php echo site_url('management/remove/levels/' . $l->id); ?>" class="btn btn-danger btn-small">Remove</a></td>
                  </tr>
                <?php $i++;}?>
              <?php } else {?>
                <tr>
                  <td colspan="2" class="text-center">No Information Available</td>
                </tr>
              <?php }?>
            </table>            
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php $this->load->view('footer');?>