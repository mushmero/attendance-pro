<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>

<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><i class="fas fa-user"></i> Management Login</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->    
  </section>
  <section class="content">
    <div class="management-login">
        <div class="login-box">
           <div class="card card-primary card-outline">
             <div class="card-body">
               <p class="login-box-msg">Please enter username & password</p>
                <?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
                <?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
                <form action="<?php echo site_url('management/login'); ?>" method="POST">
                  <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="username" placeholder="username">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    <?php echo form_error('username', '<p class="help-inline">', '</p>'); ?>
                  </div>
                  <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="password" placeholder="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <?php echo form_error('password', '<p class="help-inline">', '</p>'); ?>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                    </div>
                  </div>
                </form>
             </div>
           </div>
        </div>
    </div>
  </section>
</div>
<?php $this->load->view('footer');?>
