<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>

<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><i class="fas fa-sticky-note"></i> Management</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->    
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-4">
        <div class="card card-danger card-outline">
          <div class="card-header text-center">
            <h3 class="card-title">Registration Form</h3>
          </div>
          <div class="card-body">
            <?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
            <?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
              <form action="<?php echo site_url('management/register'); ?>" method="POST">
                <div class="form-group has-feedback">
                  <input type="text" class="form-control" name="username" placeholder="Username">
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  <?php echo form_error('username', '<p class="help-inline">', '</p>'); ?>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" class="form-control" name="password" placeholder="Password">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  <?php echo form_error('password', '<p class="help-inline">', '</p>'); ?>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" class="form-control" name="cpassword" placeholder="Retype Password">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  <?php echo form_error('cpassword', '<p class="help-inline">', '</p>'); ?>
                </div>
                <div class="form-group">
                  <select class="form-control" name="level">
                    <?php if (!empty($levels)) {?>
                      <option value="">--Please Choose--</option>
                      <?php foreach ($levels as $l) {?>
                        <option value="<?php echo $l->id; ?>"><?php echo $l->level; ?></option>
                      <?php }?>
                    <?php } else {?>
                    <option value="">No Data</option>
                    <?php }?>
                  </select>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                  </div>
                </div>
              </form>            
          </div>
        </div>
      </div>
      <div class="col-md-8">
        <div class="card card-success card-outline">
          <div class="card-header text-center">
            <h3 class="card-title">List of Registered Users</h3>
          </div>
          <div class="card-body">
            <table class="table table-condensed">
              <tr>
                <th>#</th>
                <th>Username</th>
                <th>Level</th>
                <th>Action</th>
              </tr>
              <?php if (!empty($users)) {?>
                <?php $i = 1;foreach ($users as $u) {?>
                  <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $u->username; ?></td>
                    <td><?php echo $u->level; ?></td>
                    <td><a href="<?php echo site_url('management/remove/user/' . $u->id); ?>" class="btn btn-danger btn-small">Remove</a></td>
                  </tr>
                <?php $i++;}?>
              <?php } else {?>
                <tr>
                  <td colspan="3" class="text-center">No Information Available</td>
                </tr>
              <?php }?>
            </table>            
          </div>
        </div>
      </div>
      <!-- /.login-box -->
    </div>
  </section>
</div>
<?php $this->load->view('footer');?>