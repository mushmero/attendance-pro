<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<h1><i class="fas fa-tachometer-alt"></i> Dashboard</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-green">
					<span class="info-box-icon"><i class="fas fa-user"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Normal</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-red">
					<span class="info-box-icon"><i class="fas fa-user-clock"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Late</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-orange">
					<span class="info-box-icon"><i class="fas fa-user-times"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Missing</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-teal">
					<span class="info-box-icon"><i class="fas fa-users"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Staffs</span>
						<span class="info-box-number"><?php echo $countAllStaff->count; ?></span>
					</div>
				</div>
			</div>
<!-- 			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-green">
					<span class="info-box-icon"><i class="fas fa-user"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Normal</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-red">
					<span class="info-box-icon"><i class="fas fa-user-clock"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Late</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-orange">
					<span class="info-box-icon"><i class="fas fa-user-times"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Missing</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-aqua">
					<span class="info-box-icon"><i class="fas fa-users"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Staffs</span>
						<span class="info-box-number">10</span>
					</div>
				</div>
			</div> -->
		</div>
	</section>
</div>
<?php $this->load->view('footer');?>