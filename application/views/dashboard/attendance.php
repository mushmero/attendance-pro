<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
	      <div class="row mb-2">
	        <div class="col-sm-6">
	          <h1><i class="fas fa-calendar-alt"></i> Attendance</h1>
	        </div><!-- /.col -->
	      </div><!-- /.row -->
	    </div><!-- /.container-fluid -->		
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-4">
				<div class="card card-danger card-outline">
					<div class="card-header text-center">
						<h3 class="card-title">ID Search</h3>
					</div>
					<div class="card-body">
						<?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
						<form class="form" action="<?php echo site_url('dashboard/attendance'); ?>" method="POST">
							<div class="form-group">
								<input type="text" name="staffid" class="form-control" placeholder="Please Insert Staff ID">
								<?php echo form_error('staffid', '<p class="help-inline">', '</p>'); ?>
							</div>
							<button type="submit" class="btn btn-flat btn-block btn-danger">Search</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="card card-danger card-outline">
					<div class="card-header text-center">
						<h3 class="card-title">Results</h3>
					</div>
					<div class="card-body">
						<table class="table table-condensed">
							<tr>
								<th>Date In</th>
								<th>Staff</th>
								<th>IN</th>
								<th>Date Out</th>
								<th>OUT</th>
								<th>Status</th>
							</tr>
							<?php if (!empty($result)) {?>
								<?php foreach ($result as $r) {	?>
									<?php $current = date('H:m:i', $r->timein);
									$limit = date('H:m:i', strtotime('09:00:00'));
									if ($current > $limit) {
										$status = '<span class="badge badge-danger">Late<span>';
									} else if ($current <= $limit) {
										$status = '<span class="badge badge-success">Normal<span>';}
									?>
									<tr>
										<td><?php echo $r->datein; ?></td>
										<td><?php echo $r->name; ?></td>
										<td><?php if ($r->timein != '') {echo date('h:i:s A', $r->timein);} else {echo "Not Found";}?></td>
										<td><?php echo $r->dateout; ?></td>
										<td><?php if ($r->timeout != '') {echo date('h:i:s A', $r->timeout);} else {echo "Not Found";}?></td>
										<td><?php echo $status; ?></td>
									</tr>
								<?php }?>
							<?php } else {?>
								<tr>
									<td colspan="5">No information available. Please search staff id</td>
								</tr>
							<?php }?>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info card-outline">
					<div class="card-header text-center">
						<h3 class="card-title"><strong>Today <?php echo date('d/m/Y'); ?></strong></h3>
					</div>
					<div class="card-body">
						<table class="table table-condensed">
							<tr>
								<th>#</th>
								<th>Staff</th>
								<th>In</th>
								<th>Out</th>
								<th>Status</th>
							</tr>
							<?php if (!empty($all)) {?>
								<?php $i = 1;foreach ($all as $a) {?>
								<?php $intime = date('H:m:i', $a->timein);
								$limit = date('H:m:i', strtotime('09:00:00'));
								if ($intime > $limit) {
									$status = '<span class="badge badge-danger">Late<span>';
								} else if ($intime <= $limit) {
									$status = '<span class="badge badge-success">Normal<span>';
								}?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $a->name . ' (' . $a->staffno . ')'; ?></td>
										<td><?php if ($a->timein != '') {echo date('h:i:s A', $a->timein);} else {echo "Not Found";}?></td>
										<td><?php if ($a->timeout != '') {echo date('h:i:s A', $a->timeout);} else {echo "Not Found";}?></td>
										<td><?php echo $status; ?></td>
									</tr>
								<?php $i++;}?>
							<?php } else {?>
								<tr>
									<td colspan="5">No information available</td>
								</tr>
							<?php }?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('footer');?>