<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
	    <div class="container-fluid">
	      <div class="row mb-2">
	        <div class="col-sm-6">
	          <h1><i class="fas fa-user-friends"></i> Staffs</h1>
	        </div><!-- /.col -->
	      </div><!-- /.row -->
	    </div><!-- /.container-fluid --> 		
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-4 col-12">
				<div class="card card-primary card-outline">
					<div class="card-header text-center">
						<h3 class="card-title">Staff Details</h3>
					</div>
					<div class="card-body">
						<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
              			<?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
              			<form action="<?php echo site_url('dashboard/staff'); ?>" method="POST">
              				<div class="form-group">
              					<div class="input-group mb-3">
              						<div class="input-group-prepend">
              							<span class="input-group-text">
              								<i class="fas fa-user"></i>
              							</span>
              						</div>
              						<input type="text" name="name" class="form-control" placeholder="Full Name">
                  					<?php echo form_error('username', '<p class="help-inline">', '</p>'); ?>
              					</div>
              					<div class="input-group mb-3">
              						<div class="input-group-prepend">
              							<span class="input-group-text">
              								<i class="fas fa-venus-mars"></i>
              							</span>
              						</div>
              						<select class="bs-select form-control" name="gender">
	              						<option value="">Select Option</option>
	              						<option value="male">MALE</option>
	              						<option value="female">FEMALE</option>
	              						<option value="not shared">NOT SHARED</option>
	              					</select>
	              					<?php echo form_error('gender', '<p class="help-inline">', '</p>'); ?>
              					</div>
              				</div>
              				<div class="form-group">
              					<div class="input-group mb-3">
              						<div class="input-group-prepend">
              							<span class="input-group-text">
              								<i class="fas fa-home"></i>
              							</span>
              						</div>
              						<input type="text" name="address" class="form-control" placeholder="Address">
                  					<?php echo form_error('address', '<p class="help-inline">', '</p>'); ?>
              					</div>
              				</div>
              				<div class="form-group">
              					<div class="row">
	              					<div class="col-6">
		              					<div class="input-group mb-3">
		              						<div class="input-group-prepend">
		              							<span class="input-group-text">
		              								<i class="fas fa-home"></i>
		              							</span>
		              						</div>
		              						<input type="text" name="city" class="form-control" placeholder="City">
		              						<?php echo form_error('city', '<p class="help-inline">', '</p>'); ?>
		              					</div>
		              				</div>
		              				<div class="col-6">
		              					<div class="input-group mb-3">
		              						<div class="input-group-prepend">
		              							<span class="input-group-text">
		              								<i class="fas fa-inbox"></i>
		              							</span>
		              						</div>
		              						<input type="number" name="postcode" class="form-control" placeholder="Postcode">
		                  					<?php echo form_error('username', '<p class="help-inline">', '</p>'); ?>
		              					</div>	              					
		              				</div>
		              			</div>
              				</div>
              				<div class="form-group">
              					<div class="input-group mb-3">
              						<div class="input-group-prepend">
              							<span class="input-group-text">
              								<i class="fas fa-home"></i>
              							</span>
              						</div>
              						<select class="bs-select form-control" name="states">
						                <?php if (!empty($states)) {?>
						                <option value="">Select Option</option>
						                <?php foreach ($states as $key => $value) {?>
						                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						                <?php }?>
						                <?php } else {?>
						                <option value="">No Data</option>
						                <?php }?>
						            </select>
	                  				<?php echo form_error('states', '<p class="help-inline">', '</p>'); ?>
              					</div>
              				</div>
              				<div class="form-group">
              					<div class="row">
	              					<div class="col-6">
	              						<div class="input-group mb-3">
		              						<div class="input-group-prepend">
		              							<span class="input-group-text">
		              								<i class="fas fa-building"></i>
		              							</span>
		              						</div>
		              						<select class="form-control" name="department">
								                <?php if (!empty($department)) {?>
								                <option value="">Select Option</option>
								                <?php foreach ($department as $d) {?>
								                <option value="<?php echo $d->id; ?>"><?php echo $d->deptname; ?></option>
								                <?php }?>
								                <?php } else {?>
								                <option value="">No Data</option>
								                <?php }?>
								            </select>
			                  				<?php echo form_error('department', '<p class="help-inline">', '</p>'); ?>
		              					</div>
	              					</div>
	              					<div class="col-6">
		              					<div class="input-group mb-3">
		              						<div class="input-group-prepend">
		              							<span class="input-group-text">
		              								<i class="fas fa-level-up-alt"></i>
		              							</span>
		              						</div>
		              						<select class="bs-select form-control" name="level">
						                    	<?php if (!empty($levels)) {?>
						                      	<option value="">Select Option</option>
						                      	<?php foreach ($levels as $l) {?>
						                        <option value="<?php echo $l->id; ?>"><?php echo $l->level; ?></option>
						                      	<?php }?>
						                    	<?php } else {?>
						                    	<option value="">No Data</option>
						                    	<?php }?>
						                  	</select>
						                  	<?php echo form_error('level', '<p class="help-inline">', '</p>'); ?>
		              					</div>              						
	              					</div>
	              				</div>
              				</div>
              				<div class="form-group">
              					<div class="col-12">
              						<button type="submit" class="btn btn-primary btn-flat btn-block">Register</button>
              					</div>
              				</div>
              			</form>						
					</div>
				</div>
			</div>
			<div class="col-md-8 col-12">
				<div class="card card-warning card-outline">
					<div class="card-header text-center">
						<h3 class="card-title">List of Registered Staffs</h3>
					</div>
					<div class="card-body">
						<table class="table">
							<tr>
								<th>#</th>
								<th>ID</th>
								<th>Name</th>
								<th>Department</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
							<?php if (!empty($staff)) {?>
							<?php $i = 1;foreach ($staff as $ss) {?>
								<tr>
									<td><?php echo $i; ?></td>
									<td><?php echo $ss->staffno; ?></td>
									<td><?php echo $ss->name; ?></td>
									<td><?php echo $ss->deptname; ?></td>
									<td><?php echo $ss->status; ?></td>
									<td><button class="btn btn-info btn-small" data-toggle="modal" data-target="#staff_info-<?php echo $i; ?>">Info</button> | <button class="btn btn-warning btn-small">Edit</button> | <a href="<?php echo site_url('dashboard/remove/staff/' . $ss->id); ?>" class="btn btn-danger btn-small">Remove</a></td>
							        <div class="modal fade" id="staff_info-<?php echo $i; ?>">
							          <div class="modal-dialog">
							            <div class="modal-content">
							              <div class="modal-header bg-info">
							                <h4 class="modal-title">Information on <?php echo $ss->staffno; ?></h4>
							                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							                  <span aria-hidden="true">&times;</span>
							              	</button>
							              </div>
							              <div class="modal-body">
							                <div class="row">
							                	<div class="col-4">
							                		Name:
							                	</div>
							                	<div class="col-8">
							                		<?php echo $ss->name; ?>
							                	</div>
							                </div>
							                <div class="row">
							                	<div class="col-4">
							                		Staff No:
							                	</div>
							                	<div class="col-8">
							                		<?php echo $ss->staffno; ?>
							                	</div>
							                </div>
							                <div class="row">
							                	<div class="col-4">
							                		Gender:
							                	</div>
							                	<div class="col-8">
							                		<?php echo $ss->gender; ?>
							                	</div>
							                </div>
							                <div class="row">
							                	<div class="col-4">
							                		Address:
							                	</div>
							                	<div class="col-8">
							                		<?php echo $ss->address . ' ' . $ss->postcode . ' ' . $ss->city . ' ' . $ss->states; ?>
							                	</div>
							                </div>
							                <div class="row">
							                	<div class="col-4">
							                		Department:
							                	</div>
							                	<div class="col-8">
							                		<?php echo $ss->deptname; ?>
							                	</div>
							                </div>
							                <div class="row">
							                	<div class="col-4">
							                		Level:
							                	</div>
							                	<div class="col-8">
							                		<?php echo $ss->levelName; ?>
							                	</div>
							                </div>
							              </div>
							              <div class="modal-footer">
							                <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Close</button>
							              </div>
							            </div>
							            <!-- /.modal-content -->
							          </div>
							          <!-- /.modal-dialog -->
							        </div>
								</tr>
							<?php $i++;}?>
							<?php } else {?>
								<tr>
									<td colspan="5">No information available</td>
								</tr>
							<?php }?>
						</table>						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('footer');?>