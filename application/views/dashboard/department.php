<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><i class="fas fa-building"></i> Department</h1>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid --> 		
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-4 col-12">
				<div class="card card-success card-outline">
					<div class="card-header text-center">
						<h3 class="card-title">Department Details</h3>
					</div>
					<div class="card-body">
						<?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
      			<?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
      			<form action="<?php echo site_url('dashboard/department'); ?>" method="POST">
      				<div class="form-group">
      					<div class="input-group mb-3">
      						<div class="input-group-prepend">
      							<span class="input-group-text">
      								<i class="fas fa-address-card"></i>
      							</span>
      						</div>
      						<input type="text" name="deptname" class="form-control" placeholder="Department Name">
      						<?php echo form_error('deptname', '<p class="help-inline">', '</p>') ?>
      					</div>
      				</div>
      				<div class="form-group">
      					<div class="input-group mb-3">
      						<div class="input-group-prepend">
      							<span class="input-group-text">
      								<i class="fas fa-tag"></i>
      							</span>
      						</div>
      						<input type="text" name="tag" class="form-control" placeholder="Department Tag">
      						<?php echo form_error('tag', '<p class="help-inline">', '</p>') ?>
      					</div>
      				</div>
      				<div class="form-group">
      					<div class="col-12">
      						<button type="submit" class="btn btn-danger btn-block btn-flat">Add New</button>
      					</div>
      				</div>
      			</form>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-12">
				<div class="card card-warning card-outline">
					<div class="card-header text-center">
						<h3 class="card-title">List of Departments</h3>
					</div>
					<div class="card-body">
						<table class="table table-condensed">
							<tr>
								<th>#</th>
								<th>Department Name</th>
								<th>Tag</th>
								<th>Action</th>
							</tr>
							<?php if (!empty($department)) {?>
								<?php $i = 1;foreach ($department as $d) {?>
									<tr>
										<td><?php echo $i; ?></td>
										<td><?php echo $d->deptname; ?></td>
										<td><?php echo $d->tag; ?></td>
										<td><a href="<?php echo site_url('dashboard/remove/department/' . $d->id); ?>" class="btn btn-danger btn-small">Remove</a></td>
									</tr>
								<?php $i++;}?>
							<?php } else {?>
								<tr>
									<td colspan="2">No information available</td>
								</tr>
							<?php }?>
						</table>						
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('footer');?>