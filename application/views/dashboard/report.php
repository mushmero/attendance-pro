<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>
<?php $this->load->view('sidebar');?>
<div class="content-wrapper">
	<section class="content-header">
		<div class="container-fluid">
	      <div class="row mb-2">
	        <div class="col-sm-6">
	          <h1><i class="fas fa-file"></i> Reports</h1>
	        </div><!-- /.col -->
	      </div><!-- /.row -->
	    </div><!-- /.container-fluid -->		
	</section>
	<section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-4">&nbsp;</div>
							<div class="col-4">
								<input type="hidden" id="formurl" value="<?php echo site_url('dashboard/getreportdata'); ?>">
								<form id="reportform" method="POST">
									<div class="form-group row">
										<div class="col-6">
											<select class="bs-select form-control" name="reportSelect" id="reportSelect">
												<option value="all" selected>All</option>
												<option value="other">Other</option>
			              					</select>
			              				</div>
			              				<div class="col-6">
			              					<div id="reportdate">
			                                    <div class="input-group date" id="reportdate" data-target-input="nearest">
			                                      <input type="text" class="form-control datetimepicker-input" name="reportdate" id="reportdate" data-target="#reportdate" data-toggle="datetimepicker" />
			                                      <div class="input-group-append">
			                                          <div class="input-group-text"><span class="fas fa-calendar-alt"></span></div>
			                                      </div>
			                                    </div>
			                                </div>
		                                </div>
									</div>
								</form>
							</div>
							<div class="col-4">&nbsp;</div>
						</div>
						<table class="table table-striped table-bordered" id="reportTable">
							<!-- <thead>
								<tr>
									<th width="10px">#</th>
									<th>Staff</th>
									<th>Department</th>
									<th>Level</th>
									<th>Date</th>
									<th>Time In</th>
									<th>Time Out</th>
									<th>Status</th>
								</tr>
							</thead> -->
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php $this->load->view('footer');?>