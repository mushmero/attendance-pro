<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri = $this->uri->segment(2);?>
<?php $uri2 = $this->uri->segment(1);?>
<?php $user = $this->session->userdata('user');?>

<!-- Sidebar toggle button-->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
	<ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
</nav>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="#" class="brand-link">
		<img src="<?php echo base_url('/assets/img/a-pro-logo-v1.png'); ?>" alt="Attendance-Pro Logo" class="brand-image elevation-3" style="opacity: .8">    
      	<span class="brand-text font-weight-light">Attendance-Pro</span>
    </a>
	<div class="sidebar">
		<?php if ($uri2 == 'dashboard') {?>
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
        	<div class="image">
          		<img src="<?php echo base_url('/vendor/almasaeed2010/adminlte/dist/img/avatar.png') ?>" class="img-circle  elevation-2" alt="User Image">
        	</div>
        	<div class="pull-left info">
          		<p class="text-white">Hi <b><?php echo $user['username']; ?>!</b></p>
        	</div>
     	</div>
     	<?php }?>
     	<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<?php if ($uri2 == 'dashboard') {?>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('management/logout'); ?>"><i class="nav-icon fas fa-sign-out-alt"></i> <p>Logout</p></a>
				</li>
				<?php }?>
				<li class="nav-header">E-Attendance</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url(); ?>"><i class="nav-icon fas fa-calendar-alt"></i> <p>E-Attendance</p></a>
				</li>
				<li class="nav-header">Navigation</li>
				<?php if ($uri2 == 'management' && ($uri == 'login' || $uri == 'logout')) {?>
				<li class="active nav-item">
					<a class="nav-link" href="<?php echo site_url('management/login'); ?>"><i class="nav-icon fas fa-sign-in-alt"></i> <p>Login</p></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('management/register'); ?>"><i class="nav-icon fas fa-lock"></i> <p>Register</p></a>
				</li>
				<?php } else if ($uri2 == 'management' && $uri == 'register') {?>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('management/login'); ?>"><i class="nav-icon fas fa-sign-in-alt"></i> <p>Login</p></a>
					</li>
					<li class="active nav-item">
						<a class="nav-link" href="<?php echo site_url('management/register'); ?>"><i class="nav-icon fas fa-lock"></i> <p>Register</p></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('management/level'); ?>"><i class="nav-icon fas fa-plus"></i> <p>Levels</p></a>
					</li>
				<?php } else if ($uri2 == 'management' && $uri == 'level') {?>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('management/login'); ?>"><i class="nav-icon fas fa-sign-in-alt"></i> <p>Login</p></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('management/register'); ?>"><i class="nav-icon fas fa-lock"></i> <p>Register</p></a>
					</li>
					<li class="active nav-item">
						<a class="nav-link" href="<?php echo site_url('management/level'); ?>"><i class="nav-icon fas fa-plus"></i> <p>Levels</p></a>
					</li>
				<?php }?>
				<?php if ($uri2 == 'dashboard') {?>
				<?php if ($uri == 'main') {?>
				<li class="active nav-item">
				<?php } else {?>
				<li class="nav-item">
				<?php }?>
					<a class="nav-link" href="<?php echo site_url('dashboard/main'); ?>"><i class="nav-icon fas fa-tachometer-alt"></i> <p>Dashboard</p></a>
				</li>
				<?php if ($uri == 'department') {?>
				<li class="active nav-item">
				<?php } else {?>
				<li class="nav-item">
				<?php }?>
					<a class="nav-link" href="<?php echo site_url('dashboard/department'); ?>"><i class="nav-icon fas fa-building"></i> <p>Department</p></a>
				</li>
				<?php if ($uri == 'staff') {?>
				<li class="active nav-item">
				<?php } else {?>
				<li class="nav-item">
				<?php }?>
					<a class="nav-link" href="<?php echo site_url('dashboard/staff'); ?>"><i class="nav-icon fas fa-user-friends"></i> <p>Staffs</p></a>
				</li>
				<?php if ($uri == 'attendance') {?>
				<li class="active nav-item">
				<?php } else {?>
				<li class="nav-item">
				<?php }?>
					<a class="nav-link" href="<?php echo site_url('dashboard/attendance'); ?>"><i class="nav-icon far fa-calendar-alt"></i> <p>Attendance</p></a>
				</li>
				<?php if ($uri == 'reports') {?>
				<li class="active nav-item">
				<?php } else {?>
				<li class="nav-item">
				<?php }?>
					<a class="nav-link" href="<?php echo site_url('dashboard/reports'); ?>"><i class="nav-icon fas fa-file"></i> <p>Reports</p></a>
				</li>
				<?php }?>
			</ul>
		</nav>
	</div>
</aside>