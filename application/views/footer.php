<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $uri = $this->uri->segment(1);?>

<?php if ($uri == "") {?>
<footer class="login-footer">
	<?php $color = "text-muted";?>
<?php } else {?>
<footer class="main-footer">
	<?php $color = "text-muted";?>
<?php }?>
	<p class="text-center <?php echo $color ?>"><strong><?php echo date('Y'); ?></strong> | Version: <?php echo getenv('APP_VERSION') ?> | <a href="https://mushmero.com" target="_blank">mushmero</a></p>
</footer>
</div>
<script src="<?php echo base_url('/vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('/vendor/almasaeed2010/adminlte/plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script src="<?php echo base_url('/vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('/vendor/almasaeed2010/adminlte/plugins/moment/moment.min.js') ?>"></script>
<script src="<?php echo base_url('/vendor/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.js') ?>"></script>
<script src="<?php echo base_url('/vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js'); ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.11.4/b-2.2.2/b-html5-2.2.2/b-print-2.2.2/date-1.1.1/r-2.2.9/sc-2.0.5/sb-1.3.0/datatables.min.js"></script>
<script src="<?php echo base_url('/assets/js/custom.js'); ?>"></script>
</body>
</html>