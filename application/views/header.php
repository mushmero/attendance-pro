<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width = device-width, initial-scale = 1">
	<title>Attendance Pro</title>
  	<link rel="icon" type="image/x-icon" href="<?php echo base_url('/assets/img/a-pro-logo-v1.png'); ?>">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  	<link rel="stylesheet" href="<?php echo base_url('/vendor/almasaeed2010/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'); ?>">
  	<link rel="stylesheet" href="<?php echo base_url('/vendor/almasaeed2010/adminlte/dist/css/adminlte.css'); ?>">
  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.11.4/b-2.2.2/b-html5-2.2.2/b-print-2.2.2/date-1.1.1/r-2.2.9/sc-2.0.5/sb-1.3.0/datatables.min.css"/>
  	<link rel="stylesheet" href="<?php echo base_url('/assets/css/custom.css'); ?>">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php $uri = $this->uri->segment(1);?>
<?php if ($uri == "") {?>
	<body class="hold-transition login-page home-wrapper">
<?php } else {?>
	<body class="hold-transition sidebar-mini">
		<div class="wrapper">
<?php //$this->load->view('logo');?>
<?php }?>