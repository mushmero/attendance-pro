<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('header');?>

<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-primary">
    <div class="card-header text-center">
      <img src="<?php echo base_url('/assets/img/a-pro-logo-v1.png'); ?>" alt="Attendance-Pro Logo" class="brand-image home-logo elevation-3" height="33">
      <a href="<?php echo base_url(); ?>" class="text-center"><b>E-Attendance</b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Please enter your staff no</p>
      <?php echo (isset($success)) ? "<div class=\"alert alert-success\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$success</strong></div>" : ''; ?>
      <?php echo (isset($error)) ? "<div class=\"alert alert-danger\" name=\"alert\" id=\"alert\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$error</strong></div>" : ''; ?>
      <form action="<?php echo site_url(); ?>" method="POST">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" name="staffid" placeholder="Staff No">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
          <?php echo form_error('staffid', '<p class="help-inline">', '</p>'); ?>
        </div>
        <div class="row">
          <div class="col-6">
            <button type="submit" value="1" name="in" class="btn btn-success btn-block btn-flat">IN</button>
          </div>
          <div class="col-6">
            <button type="submit" value="2" name="out" class="btn btn-danger btn-block btn-flat">OUT</button>
          </div>
        </div>
        <div>&nbsp;</div>
        <div class="row">
          <div class="col-12">
            <p class="text-center"><a href="<?php echo site_url('management/login'); ?>">Management Login</a></p>
          </div>
        </div>
      </form>
    </div>
    <!-- /.card-body -->
    <div class="card-footer home-footer">
      <?php $this->load->view('footer');?>
    </div>
  </div>
  <!-- /.card -->
</div>