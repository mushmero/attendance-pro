/*set timeout hide*/
setTimeout(function(){
    $("#alert").hide();
}, 2000);

$(document).ready(function() {
    /* report form */
    var reportForm = $('#reportform');
    var formurl = $('#formurl').val();
    /* report select drop down*/
    var reportselect = $('#reportSelect');
    if(reportselect.val() == 'all'){
      $('#reportdate').hide();
      $.ajax({
        url: formurl,
        type: 'POST',
        success: function(d){
            var dt = $.parseJSON(d);
            generateReportTable(dt);
        }
      });    
  }    
    reportselect.on('change', function(e){
        var current = $(e.currentTarget).val();
        if(current == 'other'){
            $('#reportdate').show();
            /* report date selection*/
            $('#reportdate').on('change.datetimepicker', function(e){
                $('input#reportdate').val(e.date.format('DD/MM/YYYY'));
                if( !e.oldDate || !e.date.isSame(e.oldDate, 'day')){
                    $('.bootstrap-datetimepicker-widget').hide(function(){                       
                        var params = {
                            'reportSelect': current,
                            'reportdate': $('input#reportdate').val(),
                        };
                        $.ajax({
                            type: 'POST',
                            url: formurl,
                            data: params
                        }).then(function(data){
                            reportselect.val(current);
                            $('input#reportdate').val(e.date.format('DD/MM/YYYY'));
                            var dt = $.parseJSON(data);
                            generateReportTable(dt);
                        });
                    });                       
                }
            }).datetimepicker();
        }else{
            $('#reportdate').hide(function(){
                var params = {
                    'reportSelect': current,
                };
                $.ajax({
                    type: 'POST',
                    url: formurl,
                    data: params
                }).then(function(data){
                    var dt = $.parseJSON(data);
                    generateReportTable(dt);
                });             
            });
        }
    });

    function generateReportTable(dt){
        var reportTable = $('#reportTable').DataTable({
            destroy: true,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'pageLength',
                    className: 'btn btn-block btn-default',
                },
                'spacer',
                {
                    extend:    'excelHtml5',
                    text:      '<i class="far fa-file-excel"></i> Excel',
                    titleAttr: 'Excel',
                    className: 'btn btn-info',
                },
                {
                    extend:    'csvHtml5',
                    text:      '<i class="fas fa-file-csv"></i> CSV',
                    titleAttr: 'CSV',
                    className: 'btn btn-info',
                },
                {
                    extend:    'pdfHtml5',
                    text:      '<i class="far fa-file-pdf"></i> PDF',
                    titleAttr: 'PDF',
                    className: 'btn btn-info',
                }
            ],
            data : dt,
            columns : [
                {"title" : "#"},
                {"title" : "Staff"},
                {"title" : "Department"},
                {"title" : "Level"},
                {"title" : "Date"},
                {"title" : "Time In"},
                {"title" : "Time Out"},
                {"title" : "Status"},
            ],
        });
          reportTable.buttons().container().appendTo( '#reportTable_wrapper .col-md-6:eq(0)' );
    }
} );